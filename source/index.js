import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import Router from "./Router/index";

export default function index() {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}
