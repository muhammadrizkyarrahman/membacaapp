import React, { useState, createContext, useEffect } from "react";
import * as firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCWCGrp0DdrYdrhuy1dpe-nZwvH1ZCttiM",
  authDomain: "membacaapp.firebaseapp.com",
  projectId: "membacaapp",
  storageBucket: "membacaapp.appspot.com",
  messagingSenderId: "1084538142006",
  appId: "1:1084538142006:web:42a906424c844a5fc38797",
};

// // Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const CartContext = createContext();

const db = firebase.firestore();

export const CartProvider = ({ children }) => {
  const [cart, setCart] = useState([]);
  useEffect(() => {
    const ref = db.collection("cart");
    ref.onSnapshot((query) => {
      const objs = [];
      query.forEach((doc) => {
        objs.push({
          id: doc.id,
          ...doc.data(),
        });
      });
      setCart(objs);
    });
  }, []);

  return (
    <CartContext.Provider value={[cart, setCart]}>
      {children}
    </CartContext.Provider>
  );
};
