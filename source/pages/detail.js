import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
} from "react-native";
import firebase from "firebase";

export default function Detail({ route, navigation }) {
  const { item, id, cover, title, author, detail, harga } = route.params;

  const db = firebase.firestore();

  const currencyFormat = (num) => {
    return "Rp." + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const addCart = (item) => {
    const user = firebase.auth().currentUser;
    db.collection("cart")
      .add({
        title: item.title,
        author: item.author,
        harga: item.harga,
        cover: item.cover,
        detail: item.detail,
        user_id: user.uid,
      })
      .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        Alert.alert("Buku telah dimasukkan ke dalam keranjang");
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  };

  return (
    <View styles={styles.container}>
      <ScrollView>
        <View style={styles.topDetail}>
          <Image
            style={{ width: 320, height: 300, borderRadius: 5 }}
            source={{ uri: cover }}
          />
        </View>
        <View style={styles.midDetail}>
          <Text style={styles.titleBook}>{title}</Text>
          <Text style={styles.authorBook}>{author}</Text>
          <View>
            <Text style={{ marginTop: 10, fontSize: 8, color: "grey" }}>
              harga buku :
            </Text>
            <Text style={styles.priceBook}>
              {" "}
              {currencyFormat(Number(harga))}
            </Text>
          </View>
          <Text style={styles.detailBook}>{detail}</Text>
        </View>
        <View style={styles.bottomDetail}>
          <TouchableOpacity
            style={styles.buyButton}
            onPress={() =>
              navigation.navigate("Check Out", {
                item: item,
                id: item.id,
                cover: item.cover,
                title: item.title,
                author: item.author,
                detail: item.detail,
                harga: item.harga,
              })
            }
          >
            <Text style={styles.buyText}>Beli</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.addToCart}
            onPress={() => {
              addCart(item);
            }}
          >
            <Text style={styles.addToCartText}>Add To Cart</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  topDetail: {
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
  },
  midDetail: {
    backgroundColor: "#fff",
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: "#f1f1f1",
  },
  bottomDetail: {
    backgroundColor: "#fff",
    padding: 20,
  },
  titleBook: {
    fontSize: 18,
    fontWeight: "bold",
  },
  authorBook: {
    fontSize: 12,
    color: "grey",
  },
  detailBook: {
    fontSize: 14,
  },
  priceBook: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    marginLeft: -5,
  },
  buyButton: {
    backgroundColor: "#F44336",
    width: 260,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    alignSelf: "center",
    marginBottom: 10,
  },
  addToCart: {
    borderWidth: 1,
    borderColor: "#F44336",
    width: 260,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    alignSelf: "center",
    marginBottom: 10,
  },
  buyText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
  },
  addToCartText: {
    fontSize: 18,
    color: "#F44336",
    fontWeight: "bold",
  },
});
