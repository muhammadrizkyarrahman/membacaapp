import { NavigationContainer } from "@react-navigation/native";
import React, { useState, useEffect } from "react";
import {
  Button,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  Alert,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import * as firebase from "firebase";

export default function Register({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const submit = () => {
    const data = {
      email,
      password,
    };
    console.log(data);
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        var user = userCredential.user;
        console.log(user);
        navigation.navigate("MainApp");
        Alert.alert("Registration Success");
        setEmail("");
        setPassword("");
      })
      .catch((error) => {
        var errorMessage = error.message;
        Alert.alert("Registration Failed", errorMessage);
        setPassword("");
      });
  };

  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.avatarLogo}
          source={require("../assets/Logo1.png")}
        />
      </View>
      <TextInput
        placeholder="Email"
        style={styles.input}
        value={email}
        onChangeText={(value) => setEmail(value)}
      />
      <TextInput
        placeholder="Password"
        style={styles.input}
        secureTextEntry
        value={password}
        onChangeText={(value) => setPassword(value)}
      />
      <TouchableOpacity style={styles.submitButton} onPress={submit}>
        <Text style={styles.textSubmitButton}>Register</Text>
      </TouchableOpacity>
      <View style={{ flexDirection: "row" }}>
        <Text style={{ fontSize: 12, fontWeight: "bold" }}>
          Sudah punya akun?
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <Text style={{ fontSize: 12, fontWeight: "bold", color: "#F44336" }}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  input: {
    width: 260,
    height: 40,
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: "#f1f1f1",
    borderRadius: 5,
    marginBottom: 20,
  },
  submitButton: {
    width: 260,
    height: 40,
    backgroundColor: "#F44336",
    borderRadius: 5,
    marginVertical: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  textSubmitButton: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff",
  },
  avatarLogo: {
    width: 200,
    height: 200,
    marginBottom: 20,
  },
});
