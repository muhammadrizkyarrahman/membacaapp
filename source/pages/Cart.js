import React from "react";
import CartList from "../components/CartList";
import { CartProvider } from "../context/CartContext";

export default function Cart() {
  return (
    <>
      <CartProvider>
        <CartList />
      </CartProvider>
    </>
  );
}
