import React from "react";
import BookList from "../components/BookList";
import { BookProvider } from "../context/BookContext";

export default function Home() {
  return (
    <>
      <BookProvider>
        <BookList />
      </BookProvider>
    </>
  );
}
