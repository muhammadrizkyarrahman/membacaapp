import React, { useState, useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import * as firebase from "firebase";
import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const db = firebase.firestore();

export default function Profile({ navigation }) {
  const [user, setUser] = useState({});

  const [order, setOrder] = useState({});
  useEffect(() => {
    const ref = db.collection("order");
    ref.onSnapshot((query) => {
      const objs = [];
      query.forEach((doc) => {
        objs.push({
          id: doc.id,
          ...doc.data(),
        });
      });
      setOrder(objs);
    });
  }, []);

  const [cart, setCart] = useState([]);
  useEffect(() => {
    const ref = db.collection("cart");
    ref.onSnapshot((query) => {
      const objs = [];
      query.forEach((doc) => {
        objs.push({
          id: doc.id,
          ...doc.data(),
        });
      });
      setCart(objs);
    });
  }, []);

  useEffect(() => {
    const userInfo = firebase.auth().currentUser;
    setUser(userInfo);
  }, []);

  const onLogout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        console.log("user Sign out");
        navigation.navigate("Login");
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.topPage}>
        <Ionicons name="person-circle" size={100} color="grey" />
        <Text style={styles.userEmail}>{user.email}</Text>
        <Text style={styles.username}>Username</Text>
        <TouchableOpacity style={styles.logoutButton} onPress={onLogout}>
          <Text style={styles.textLogoutButton}>Logout</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bottomPage}>
        <View style={styles.cartContainer}>
          <View style={styles.cartIcon}>
            <Ionicons name="cart-outline" size={50} color="white" />
          </View>
          <View style={styles.dataCart}>
            <Text style={styles.textDataCart}>Isi keranjang</Text>
            <Text style={styles.textDataCart}>{cart.length}</Text>
          </View>
          <TouchableOpacity
            style={styles.buttonCartArea}
            onPress={() => navigation.navigate("Cart")}
          >
            <Text style={{ fontSize: 14, fontWeight: "bold", color: "#fff" }}>
              Lihat
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.cartContainer}>
          <View style={styles.cartIcon}>
            <MaterialCommunityIcons name="cube-send" size={50} color="white" />
          </View>
          <View style={styles.dataCart}>
            <Text style={styles.textDataCart}>Pesanan saya</Text>
            <Text style={styles.textDataCart}>{order.length}</Text>
          </View>
          <TouchableOpacity
            style={styles.buttonCartArea}
            onPress={() => navigation.navigate("Pesanan saya")}
          >
            <Text style={{ fontSize: 14, fontWeight: "bold", color: "#fff" }}>
              Lihat
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  topPage: {
    flex: 1,
    padding: 20,
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#f1f1f1",
  },
  bottomPage: {
    flex: 1,
    padding: 20,
    alignItems: "center",
    backgroundColor: "#f1f1f1",
  },
  userEmail: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#000",
  },
  username: {
    fontSize: 12,
    color: "grey",
  },
  logoutButton: {
    borderWidth: 1,
    width: 120,
    height: 40,
    borderColor: "#F44336",
    borderRadius: 5,
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  textLogoutButton: {
    color: "#F44336",
    fontWeight: "bold",
  },
  cartContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#fff",
    borderColor: "#5c6bc0",
    // borderWidth: 1,
    borderLeftWidth: 10,
    borderRadius: 5,
    width: "100%",
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 10,
  },
  cartIcon: {
    width: 70,
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    backgroundColor: "#fff",
    // borderWidth: 3,
    backgroundColor: "#F44336",
    marginRight: 20,
  },
  dataCart: {
    flex: 3,
    borderColor: "grey",
  },
  textDataCart: {
    color: "#000",
    fontSize: 14,
    fontWeight: "bold",
  },
  buttonCartArea: {
    backgroundColor: "#F44336",
    width: 60,
    height: 40,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
});
