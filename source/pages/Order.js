import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import OrderList from "../components/OrderList";
import { OrderProvider } from "../context/OrderContext";

export default function Order() {
  return (
    <>
      <OrderProvider>
        <OrderList />
      </OrderProvider>
      {/* <Text>Order</Text> */}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
});
