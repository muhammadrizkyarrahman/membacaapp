import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
} from "react-native";
import firebase from "firebase";
import { Ionicons } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";

export default function CheckOut({ route }) {
  const { item, id, cover, title, author, detail, harga } = route.params;

  const db = firebase.firestore();

  const navigation = useNavigation();

  const currencyFormat = (num) => {
    return "Rp." + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const addItem = (item) => {
    const user = firebase.auth().currentUser;
    db.collection("order")
      .add({
        title: title,
        author: author,
        harga: parseInt(harga) + 6900,
        status: "Sedang diproses..",
        cover: cover,
        detail: detail,
        user_id: user.uid,
      })
      .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        Alert.alert("Pesanan anda akan diproses");
        navigation.navigate("Pesanan saya");
        db.collection("cart")
          .doc(id)
          .delete()
          .then(() => {
            console.log("Document successfully deleted!");
          })
          .catch((error) => {
            console.error("Error removing document: ", error);
          });
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  };

  return (
    <View styles={styles.container}>
      <ScrollView style={{ backgroundColor: "#fff" }}>
        <View style={styles.topDetail}>
          <Image
            style={{ width: 60, height: 60, borderRadius: 5 }}
            source={{ uri: cover }}
          />
          <View style={styles.textArea}>
            <Text style={styles.titleBook}>{title}</Text>
            <Text style={styles.authorBook}>{author}</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.buttonArea}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Ionicons name="map" size={24} color="black" />
            </View>
            <View style={{ flex: 6 }}>
              <Text>Tambahkan Alamat</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Ionicons
                name="arrow-forward-circle-outline"
                size={24}
                color="black"
              />
            </View>
          </View>
        </TouchableOpacity>
        <Text style={{ marginLeft: 20, fontWeight: "bold" }}>
          Pilih metode pembayaran
        </Text>
        <ScrollView horizontal>
          <TouchableOpacity style={styles.buyMethod}>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <MaterialIcons name="payments" size={24} color="black" />
              </View>
              <View style={{ flex: 6, marginLeft: 5 }}>
                <Text>Bayar di tempat</Text>
                <Text style={{ fontSize: 10, color: "grey" }}>
                  Bayar ketika barang sampai
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buyMethod}>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <MaterialIcons name="payment" size={24} color="black" />
              </View>
              <View style={{ flex: 6 }}>
                <Text>Tambah kartu baru</Text>
                <Text style={{ fontSize: 10, color: "grey" }}>
                  Tambah kartu
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
        <View style={{ marginVertical: 20 }}>
          <Text style={{ marginLeft: 20, fontWeight: "500" }}>
            harga buku : {currencyFormat(Number(harga))}
          </Text>
          <Text style={{ marginLeft: 20, fontWeight: "500" }}>
            Biaya ongkir : {currencyFormat(Number(6900))}
          </Text>
        </View>
        <View style={styles.bottomDetail}>
          <Text style={{ fontWeight: "bold" }}>Total :</Text>
          <Text style={styles.priceBook}>
            {" "}
            {currencyFormat(Number(harga) + 6900)}
          </Text>
          <TouchableOpacity
            style={styles.buyButton}
            onPress={() => {
              addItem(item);
            }}
          >
            <Text style={styles.buyText}>Beli</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  topDetail: {
    flexDirection: "row",
    backgroundColor: "#fff",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 20,
  },
  buttonArea: {
    flex: 1,
    alignItems: "center",
    margin: 20,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#5c6bc0",
    padding: 20,
  },
  buyMethod: {
    flex: 1,
    alignItems: "center",
    marginLeft: 20,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#5c6bc0",
    padding: 20,
    marginVertical: 20,
  },
  bottomDetail: {
    backgroundColor: "#fff",
    padding: 20,
    borderTopWidth: 1,
    borderTopColor: "#f1f1f1",
  },
  textArea: {
    marginLeft: 15,
  },
  titleBook: {
    fontSize: 18,
    fontWeight: "bold",
  },
  authorBook: {
    fontSize: 12,
    color: "grey",
    marginBottom: 20,
  },
  detailBook: {
    fontSize: 14,
  },
  priceBook: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 10,
  },
  buyButton: {
    backgroundColor: "#F44336",
    width: 260,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    alignSelf: "center",
  },
  buyText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
  },
});
