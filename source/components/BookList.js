import React, { useContext } from "react";
import { useNavigation } from "@react-navigation/core";
import firebase from "firebase";

import {
  Alert,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { BookContext } from "../context/BookContext";

export default function BookList() {
  const [book] = useContext(BookContext);

  const db = firebase.firestore();

  const navigation = useNavigation();

  const currencyFormat = (num) => {
    return "Rp." + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const addCart = (item) => {
    const user = firebase.auth().currentUser;
    db.collection("cart")
      .add({
        title: item.title,
        author: item.author,
        harga: item.harga,
        cover: item.cover,
        detail: item.detail,
        user_id: user.uid,
      })
      .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        Alert.alert("Buku telah dimasukkan ke dalam keranjang");
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  };

  return (
    <>
      <View style={styles.list}>
        <FlatList
          data={book}
          numColumns={2}
          renderItem={({ item }) => (
            <View style={styles.listContainer}>
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() =>
                  navigation.navigate("Detail", {
                    item: item,
                    id: item.id,
                    cover: item.cover,
                    title: item.title,
                    author: item.author,
                    detail: item.detail,
                    harga: item.harga,
                  })
                }
              >
                <Image
                  style={{
                    width: 135,
                    height: 130,
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    marginBottom: 10,
                  }}
                  source={{ uri: item.cover }}
                />
              </TouchableOpacity>
              <View
                style={{
                  flex: 2,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={styles.titleBook}>{item.title}</Text>
                <Text style={styles.bookAuthor}>{item.author}</Text>
                <Text style={styles.price}>
                  {" "}
                  {currencyFormat(Number(item.harga))}
                </Text>
              </View>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <TouchableOpacity
                  style={styles.buyButton}
                  onPress={() =>
                    navigation.navigate("Check Out", {
                      id: item.id,
                      cover: item.cover,
                      title: item.title,
                      author: item.author,
                      detail: item.detail,
                      harga: item.harga,
                    })
                  }
                >
                  <Text
                    style={{ fontSize: 12, fontWeight: "bold", color: "#fff" }}
                  >
                    Beli
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    addCart(item);
                  }}
                  style={styles.addCart}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      fontWeight: "bold",
                      color: "#F44336",
                    }}
                  >
                    Add to Cart
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  listContainer: {
    borderRadius: 8,
    backgroundColor: "#fff",
    padding: 15,
    marginHorizontal: 5,
    marginVertical: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  buyButton: {
    backgroundColor: "#F44336",
    height: 30,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginVertical: 3,
  },
  addCart: {
    borderWidth: 1,
    borderColor: "#F44336",
    height: 30,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 3,
  },
  titleBook: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    maxWidth: 130,
  },
  bookAuthor: {
    fontSize: 10,
    color: "grey",
    textAlign: "center",
  },
  price: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#000",
    textAlign: "center",
  },
});
