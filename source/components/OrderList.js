import React, { useContext } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { OrderContext } from "../context/OrderContext";
import { useNavigation } from "@react-navigation/core";

export default function OrderList() {
  const [order] = useContext(OrderContext);

  const navigation = useNavigation();

  const currencyFormat = (num) => {
    return "Rp." + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  return (
    <>
      <View style={styles.list}>
        <FlatList
          data={order}
          numColumns={1}
          renderItem={({ item }) => (
            <View style={styles.listContainer}>
              <View style={{ flex: 1 }}>
                <Image
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 5,
                    marginBottom: 10,
                  }}
                  source={{ uri: item.cover }}
                />
              </View>
              <View
                style={{
                  flex: 2,
                  justifyContent: "center",
                  alignItems: "flex-start",
                  marginLeft: 5,
                }}
              >
                <Text style={styles.titleBook}>{item.title}</Text>
                <Text style={styles.bookAuthor}>{item.author}</Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("Home");
                  }}
                >
                  <Text style={{ fontStyle: "italic", color: "#F44336" }}>
                    Go to Home
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ alignItems: "flex-end" }}>
                <Text style={styles.price}>
                  {" "}
                  {currencyFormat(Number(item.harga))}
                </Text>
                <Text style={styles.bookAuthor}>{item.status}</Text>
              </View>
            </View>
          )}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: "#fff",
  },
  listContainer: {
    flexDirection: "row",
    backgroundColor: "#fff",
    padding: 15,
    justifyContent: "center",
    alignItems: "flex-start",
    borderBottomWidth: 1,
    borderBottomColor: "#f1f1f1",
  },
  buyButton: {
    backgroundColor: "#F44336",
    height: 30,
    width: 60,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginRight: 3,
  },
  deleteButton: {
    borderWidth: 1,
    borderColor: "#F44336",
    height: 30,
    width: 30,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
  },
  titleBook: {
    fontSize: 14,
    fontWeight: "bold",
    maxWidth: 130,
  },
  bookAuthor: {
    fontSize: 10,
    color: "grey",
  },
  price: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#000",
  },
});
