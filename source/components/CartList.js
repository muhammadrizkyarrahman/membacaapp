import React, { useContext } from "react";
import { useNavigation } from "@react-navigation/core";
import firebase from "firebase";
import { Ionicons } from "@expo/vector-icons";

import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { CartContext } from "../context/CartContext";

export default function CartList() {
  const [cart, setCart] = useContext(CartContext);

  const db = firebase.firestore();

  const navigation = useNavigation();

  const currencyFormat = (num) => {
    return "Rp." + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const deleteList = (id) => {
    db.collection("cart")
      .doc(id)
      .delete()
      .then(() => {
        console.log("Document successfully deleted!");
      })
      .catch((error) => {
        console.error("Error removing document: ", error);
      });
    setCart((currentCart) => {
      return currentCart.filter((item) => item.id !== id);
    });
  };

  return (
    <>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 20,
          paddingVertical: 10,
          backgroundColor: "#fff",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
          }}
        >
          <Ionicons name="cart-outline" size={35} color="black" />
          <Text style={{ fontSize: 18, fontWeight: "bold" }}>Cart</Text>
        </View>
      </View>
      <View style={styles.list}>
        <FlatList
          data={cart}
          numColumns={1}
          renderItem={({ item }) => (
            <View style={styles.listContainer}>
              <View style={{ flex: 1 }}>
                <Image
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 5,
                    marginBottom: 10,
                  }}
                  source={{ uri: item.cover }}
                />
              </View>
              <View
                style={{
                  flex: 2,
                  justifyContent: "center",
                  alignItems: "flex-start",
                  marginLeft: 5,
                }}
              >
                <Text style={styles.titleBook}>{item.title}</Text>
                <Text style={styles.bookAuthor}>{item.author}</Text>
                <Text style={styles.price}>
                  {" "}
                  {currencyFormat(Number(item.harga))}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <TouchableOpacity
                  style={styles.buyButton}
                  onPress={() =>
                    navigation.navigate("Check Out", {
                      id: item.id,
                      cover: item.cover,
                      title: item.title,
                      author: item.author,
                      detail: item.detail,
                      harga: item.harga,
                    })
                  }
                >
                  <Text
                    style={{ fontSize: 12, fontWeight: "bold", color: "#fff" }}
                  >
                    Beli
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    deleteList(item.id);
                  }}
                  style={styles.deleteButton}
                >
                  <Ionicons name="trash-outline" size={24} color="#F44336" />
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: "#fff",
  },
  listContainer: {
    flexDirection: "row",
    backgroundColor: "#fff",
    padding: 15,
    justifyContent: "center",
    alignItems: "flex-start",
    borderBottomWidth: 1,
    borderBottomColor: "#f1f1f1",
  },
  buyButton: {
    backgroundColor: "#F44336",
    height: 30,
    width: 60,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginRight: 3,
  },
  deleteButton: {
    borderWidth: 1,
    borderColor: "#F44336",
    height: 30,
    width: 30,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
  },
  titleBook: {
    fontSize: 14,
    fontWeight: "bold",
    maxWidth: 130,
  },
  bookAuthor: {
    fontSize: 10,
    color: "grey",
  },
  price: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#000",
  },
});
