import React from "react";
import { Image, StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { StatusBar } from "expo-status-bar";
import { Ionicons } from "@expo/vector-icons";

import Login from "../pages/Login";
import Register from "../pages/Register";
import Home from "../pages/Home";
import Profile from "../pages/Profile";
import Detail from "../pages/detail";
import Cart from "../pages/Cart";
import CheckOut from "../pages/CheckOut";
import Order from "../pages/Order";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Router() {
  return (
    <>
      <StatusBar translucent={false} backgroundColor="transparent" />
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Cart"
          component={Cart}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Detail" component={Detail} />
        <Stack.Screen name="Check Out" component={CheckOut} />
        <Stack.Screen name="Pesanan saya" component={Order} />
        <Stack.Screen
          name="MainApp"
          component={MainApp}
          options={{
            headerLeft: null,

            headerTitle: () => (
              <Image
                style={{ height: 33, width: 105, alignSelf: "center" }}
                source={require("../assets/Logo2.png")}
              />
            ),
          }}
        />
      </Stack.Navigator>
    </>
  );
}

const MainApp = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === "Home") {
            iconName = focused ? "ios-home-outline" : "ios-home-outline";
          } else if (route.name === "Cart") {
            iconName = focused ? "cart-outline" : "cart-outline";
          } else if (route.name === "Profile") {
            iconName = focused ? "ios-person-outline" : "ios-person-outline";
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: "#F44336",
        inactiveTintColor: "gray",
      }}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Cart" component={Cart} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};
