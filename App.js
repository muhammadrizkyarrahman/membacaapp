import React from "react";
import { StyleSheet } from "react-native";
import Apps from "./source/index";
import firebase from "firebase/app";
import { LogBox } from "react-native";

LogBox.ignoreLogs(["Setting a timer"]);
// // Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCWCGrp0DdrYdrhuy1dpe-nZwvH1ZCttiM",
  authDomain: "membacaapp.firebaseapp.com",
  projectId: "membacaapp",
  storageBucket: "membacaapp.appspot.com",
  messagingSenderId: "1084538142006",
  appId: "1:1084538142006:web:42a906424c844a5fc38797",
};

// // Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function App() {
  return <Apps />;
}
